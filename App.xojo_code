#tag Class
Protected Class App
Inherits WebApplication
	#tag Event
		Function HandleSpecialURL(Request As WebRequest) As Boolean
		  Select Case Request.Path
		    
		  Case "quote"
		    
		    // Output a quote
		    
		    Dim quote As String
		    quote = mQuote(Randomizer.InRange(0, mQuote.Ubound))
		    
		    Request.Print("<!DOCTYPE html>" + EndOfLine)
		    Request.Print("<html>" + EndOfLine)
		    Request.Print("<head>" + EndOfLine)
		    Request.Print("<title>Winston Churchill's Quote</title>" + EndOfLine)
		    Request.Print("</head>" + EndOfLine)
		    Request.Print("<body>" + EndOfLine)
		    Request.Print("<h1>Winston's Cherchill's Quote</h1>" + EndOfLine)
		    Request.Print("<h3>" + quote + "</h3>" + EndOfLine)
		    Request.Print("</body>" + EndOfLine)
		    Request.Print("</html>")
		    
		    // Return true to signal that we handled the request
		    Return True
		    
		  Case Else
		    // Don't handle any other requests
		    Return False
		  End Select
		End Function
	#tag EndEvent

	#tag Event
		Sub Open(args() as String)
		  mQuote.Append("If You’re Going Through Hell, Keep Going.")
		  mQuote.Append("To improve is to change; to be perfect is to change often.")
		  mQuote.Append("However beautiful the strategy, you should occasionally look at the results.")
		  mQuote.Append("We are masters of the unsaid words, but slaves of those we let slip out.")
		  mQuote.Append("Never, never, never give up.")
		  
		  Randomizer = New Random
		End Sub
	#tag EndEvent


	#tag Note, Name = LICENSE
		
		Licensed under the Apache License, Version 2.0
	#tag EndNote


	#tag Property, Flags = &h21
		Private mQuote() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Randomizer As Random
	#tag EndProperty


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
